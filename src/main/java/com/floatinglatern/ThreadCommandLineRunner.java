package com.floatinglatern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Component
public class ThreadCommandLineRunner implements CommandLineRunner, DisposableBean {

    private static Logger LOG = LoggerFactory.getLogger(ThreadCommandLineRunner.class);
    private ExecutorService executorService;

    @Override
    public void run(String... args) throws Exception {
        LOG.info("Staring ThreadCommandLineRunner");
        executorService = Executors.newSingleThreadExecutor();
        LongRunningThread longRunningThread = new LongRunningThread();

        // Execute thread uing executor service
        executorService.execute(new Thread(longRunningThread));
    }

    @Override
    public void destroy() throws Exception{
        executorService.shutdownNow();
        executorService.awaitTermination(10, TimeUnit.SECONDS);
    }

}
