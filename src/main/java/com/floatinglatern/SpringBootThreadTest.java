package com.floatinglatern;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootThreadTest {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootThreadTest.class, args);
    }
}
