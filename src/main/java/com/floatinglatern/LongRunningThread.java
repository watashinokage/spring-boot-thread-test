package com.floatinglatern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicBoolean;

public class LongRunningThread implements Runnable {

    private static Logger LOG = LoggerFactory.getLogger(LongRunningThread.class);

    @Override
    public void run() {

        LOG.info("Starting LongRunningThread with Exceptions");

        while (!Thread.interrupted()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                LOG.info("LongRunningThread processing InterruptedException");
                break;
            }
            // do something
            System.out.println("Thread doing something");
        }
        LOG.info("Ending LongRunningThread");
    }
}
